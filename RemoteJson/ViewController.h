//
//  ViewController.h
//  RemoteJson
//
//  Created by Jumpei Katayama on 2/13/15.
//  Copyright (c) 2015 Jumpei Katayama. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController<UITableViewDataSource, UITableViewDelegate>
{
    NSURLConnection *connction;
    NSMutableArray *categories;
    NSMutableData *myData;
}


@end
