//
//  ViewController.m
//  RemoteJson
//
//  Created by Jumpei Katayama on 2/13/15.
//  Copyright (c) 2015 Jumpei Katayama. All rights reserved.
//

#import "ViewController.h"
# define URLSTRING @"https://gist.githubusercontent.com/jj1100x/dabe71d9b3428efd6a66/raw/a7296f1f55d60025df0ddb70266eef8eae4935ac/product_categories.json"

@interface ViewController ()
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    categories = [[NSMutableArray alloc] init];
    [self launchUrlString:URLSTRING];
}

- (void)launchUrlString:(NSString *)urlString {
    NSLog(@"launchUrlString called");
    
    dispatch_queue_t myQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0);
    dispatch_async(myQueue, ^{ // download josn fle asyncronously
        NSURL *url = [NSURL URLWithString:urlString];
        NSData *jsonData = [NSData dataWithContentsOfURL:url];
        if(jsonData != nil)
        {
            NSError *error = nil;
            NSDictionary *result = [NSJSONSerialization JSONObjectWithData:jsonData options:NSJSONReadingAllowFragments error:&error];
            NSArray *categoriesArr = [result objectForKey:@"categories"];
            for (NSString *category in categoriesArr) {
                
                [categories addObject:category];
            }
            dispatch_async(dispatch_get_main_queue(), ^{
                [self.tableView reloadData];
            });
        }
    });
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    NSLog(@"numberofrow is called");

    return [categories count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"category cell"];

    
    cell.textLabel.text = categories[indexPath.row];
    return cell;
    
}



@end
