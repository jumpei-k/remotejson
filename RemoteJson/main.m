//
//  main.m
//  RemoteJson
//
//  Created by Jumpei Katayama on 2/13/15.
//  Copyright (c) 2015 Jumpei Katayama. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
