# RemoteJson #
To show remote JSON data on tableView using dispatch.

## Procedure ##
1. Fetch JSON data
2. Create NSData from JSON
3. Create objective-c collection from data object
4. Parse the entire JSON structure, iterating over the collection to populate data for tableView.
5. Reload table to present data

Step 1~4 should be executed in a background queue because the operations prevent you from accessing UI.


## Code ##
```objective-C
    dispatch_queue_t myQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0);
    dispatch_async(myQueue, ^{ // download josn fle asyncronously
        NSURL *url = [NSURL URLWithString:urlString];
        NSData *jsonData = [NSData dataWithContentsOfURL:url];
        if(jsonData != nil)
        {
            NSError *error = nil;
            NSDictionary *result = [NSJSONSerialization JSONObjectWithData:jsonData options:NSJSONReadingAllowFragments error:&error];
            NSArray *categoriesArr = [result objectForKey:@"categories"];
            for (NSString *category in categoriesArr) {
                
                [categories addObject:category];
            }
            dispatch_async(dispatch_get_main_queue(), ^{
                [self.tableView reloadData];
            });
        }
    });
}
```